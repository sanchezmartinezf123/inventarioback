<?php

require_once "get.model.php";


class Connection{
    static public function infoDatabase(){
        $infoDB = array(
            "database" => "inventario",
            "user" => "root",
            "pass" => ""
        );
        return $infoDB;
    }

    static public function connect(){
        try {
            $link = new PDO(
                "mysql:host=localhost;dbname=".Connection::infoDatabase()["database"],
                Connection::infoDatabase()["user"],
                Connection::infoDatabase()["pass"]
            );
            $link->exec("set names utf8");
        } catch (PDOException $e) {
            die("Error: ".$e.getMessage());
        }
        return $link;
    }

    static public function getColumnsData($table){
        $database = Connection::infoDatabase()["database"];

        return Connection::connect()
        ->query("SELECT COLUMN_NAME AS item FROM information_schema.columns WHERE table_schema = '$database' AND table_name = '$table' ")
        ->fetchAll(PDO::FETCH_OBJ);
    }

    //Generar Token de Auth
    static public function jwt($id,$email){
        $time = time();
        
        $token = array(
            "iat" => $time,
            "exp" => $time * (60*60),
            "data" => [
                "id" => $id,
                "email" => $email
            ]
        );
        
        return $token;
    }

    //Validar Token de seguridad
     static public function tokenValido($token,$table,$sufix){

        //Traemos el token de acuerdo al usuarios
        $user = GetModel::getDataFilter(
            $table,
            "token_exp_".$sufix,
            "token_".$sufix,
            $token,
            null,
            null,
            null,
            null
        );
        if (!empty($user)) {

            //Validamos que el token no haya expirado
            $time = time();
            if ($time < $user[0]->{"token_exp_".$sufix}) {
                return "Ok";
            }else{
                return "Expirado";
            }
        }else{
            return "No autorizado";
        }
     }

   

}

?>