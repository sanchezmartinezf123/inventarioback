<?php

require_once "connection.php";

class DeleteModel{

    static public function deleteData($table,$id,$nameId){


        $sql = "DELETE FROM $table WHERE $nameId =:$nameId";
        $stmt = Connection::connect()->prepare($sql);
        $stmt->bindParam(":".$nameId,$id,PDO::PARAM_STR);
        if ($stmt -> execute()){
            $response = array(
                "comment" => "Dato Eliminado"
            );
            return $response;
        }else{
            return Connection::connect()->erroInfo();
        }
    }

}

?>