<?php

require_once "connection.php";

class GetModel{
    static public function getData(
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        if (empty(Connection::getColumnsData($table))) {
            return null;
        }

        $sql = "SELECT $select FROM $table";

        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table LIMIT $startAt, $endAt";
        }


        $stmt = Connection::connect()->prepare($sql);
        $stmt -> execute();

        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    static public function getDataFilter(
        $table,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
            if (empty(Connection::getColumnsData($table))) {
                return null;
            }
        $linkToArray = explode(",",$linkTo);
        $equalToArray = explode(",",$equalTo);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText";
        
        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] = :$linkToArray[0] $linkToText LIMIT $startAt, $endAt";
        }

        $stmt = Connection::connect()->prepare($sql);
        
        foreach ($linkToArray as $key => $value) {
            $stmt -> bindParam(':'.$value,$equalToArray[$key],PDO::PARAM_STR);
        }
        $stmt -> execute();

        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    static public function getRelData(

        $rel,
        $type,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if (empty(Connection::getColumnsData($value))) {
                    return null;
                }
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText LIMIT $startAt, $endAt";
            }


            $stmt = Connection::connect()->prepare($sql);
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

    static public function getRelDataFilter(
        $rel,
        $type,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $gm
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);
        $linkToArray = explode(",",$linkTo);
        $equalToArray = explode(",",$equalTo);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                if (empty(Connection::getColumnsData($value))) {
                    return null;
                }
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText GROUP By $gm ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] = :$linkToArray[0] $linkToText LIMIT $startAt, $endAt";
            }


            $stmt = Connection::connect()->prepare($sql);
            foreach ($linkToArray as $key => $value) {
                $stmt -> bindParam(':'.$value,$equalToArray[$key],PDO::PARAM_STR);
            }
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

    static public function getDataSearch(
        $table,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        $linkToArray = explode(",",$linkTo);
        $searchArray = explode("_",$search);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                if($key > 0){
                    
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText";

        if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode";
        }

        if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
        }

        if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
            $sql = "SELECT $select FROM $table WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText LIMIT $startAt, $endAt";
        }


        $stmt = Connection::connect()->prepare($sql);
        foreach ($linkToArray as $key => $value) {
            if ($key > 0) {
                $stmt -> bindParam(':'.$value,$searchArray[$key],PDO::PARAM_STR);
            }
        }
        $stmt -> execute();

        return $stmt -> fetchAll(PDO::FETCH_CLASS);
    }

    static public function getRelDataSearch(
        $rel,
        $type,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){

        $relArray = explode(",", $rel);
        $typeArray = explode(",", $type);
        $linkToArray = explode(",",$linkTo);
        $searchArray = explode("_",$search);
        $linkToText = "";

        if (count($linkToArray)>1) {
            foreach ($linkToArray as $key => $value) {
                
                if($key > 0){
                    $linkToText .= "AND " .$value." = :".$value." "; 
                }
            }
        }

        $innerJoinText = "";

        if (count($relArray)>1) {
            foreach ($relArray as $key => $value) {
                if (empty(Connection::getColumnsData($value))) {
                    return null;
                }
                if($key > 0){
                    $innerJoinText .= "INNER JOIN ".$value." ON ".$relArray[0].".id_".$typeArray[$key]."_".$typeArray[0]." = 
                    ".$value.".id_".$typeArray[$key]." "; 
                }
            }

            

            $sql = "SELECT $select FROM $relArray[0] $innerJoinText WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText";

            if ($orderBy != null && $orderMode != null && $startAt == null && $endAt == null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode";
            }

            if ($orderBy != null && $orderMode != null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText ORDER BY $orderBy $orderMode LIMIT $startAt, $endAt";
            }

            if ($orderBy == null && $orderMode == null && $startAt != null && $endAt != null) {
                $sql = "SELECT $select FROM $relArray[0] $innerJoinText  WHERE $linkToArray[0] LIKE '%$searchArray[0]%' $linkToText LIMIT $startAt, $endAt";
            }


            $stmt = Connection::connect()->prepare($sql);
            foreach ($linkToArray as $key => $value) {
                if ($key > 0) {
                    $stmt -> bindParam(':'.$value,$searchArray[$key],PDO::PARAM_STR);
                }
            }
            $stmt -> execute();

            return $stmt -> fetchAll(PDO::FETCH_CLASS);
        }else{
            return null;
        }
    }

}

?>