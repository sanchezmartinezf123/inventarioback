<?php

require_once "models/connection.php";

$routesArray = explode("/", $_SERVER['REQUEST_URI']);
$routesArray = array_filter($routesArray);


if(count($routesArray) == 0){

    $json = array(
        'status' => 404,
        'res' => 'Not found'
    );
    
    echo json_encode($json,http_response_code($json["status"]));
    
    return; 

}

if(count($routesArray) == 4 && isset($_SERVER['REQUEST_METHOD'])){
    if($_SERVER['REQUEST_METHOD'] == "GET"){
       include ('service/get.php');
    }

    if($_SERVER['REQUEST_METHOD'] == "PUT"){
        include ('service/put.php');
    }
    
    if($_SERVER['REQUEST_METHOD'] == "POST"){
        include ('service/post.php');
    }

    if($_SERVER['REQUEST_METHOD'] == "DELETE"){
        include ('service/delete.php');
    }

}


?>