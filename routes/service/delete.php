<?php
require_once("controllers/delete.controller.php");

if (isset($_GET["id"]) && isset($_GET["nameId"])) {
    $table = explode("?", $routesArray[4])[0];
    
    if(isset($_GET["token"])){
        $tableToken = $_GET["table"];
        $sufix = $_GET["sufix"];
        $valido = Connection::tokenValido($_GET["token"],$tableToken,$sufix);
        if($valido == "Ok"){
            $response = new DeleteController();
            $response -> deleteData($table,$_GET["id"],$_GET["nameId"]);
        }
        if($valido == "Expirado"){
            $json = array(
                'status' => 303,
                'res' => 'Error El token ha expirado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
        if($valido == "No autorizado"){
            $json = array(
                'status' => 400,
                'res' => 'Error El usuario no es autorizado'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
    }else{
        $json = array(
            'status' => 400,
            'res' => 'Autorización requerida'
        );
        echo json_encode($json,http_response_code($json["status"]));
        return;
    }
}

?>