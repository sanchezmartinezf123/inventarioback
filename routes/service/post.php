<?php
require_once "models/connection.php";
require_once("controllers/post.controller.php");

if(isset($_POST)){

    $table = explode("?", $routesArray[4])[0];

    
    //Registro de usuario
    if (isset($_GET["register"]) && $_GET["register"] == true) {
        $sufix = $_GET["sufix"] ?? "usuario";
        $response = new PostController();
        $response -> postRegister($table,$_POST,$sufix);
    }
    elseif (isset($_GET["login"]) && $_GET["login"] == true) {
        $sufix = $_GET["sufix"] ?? "usuario";
        $response = new PostController();
        $response -> postLogin($table,$_POST,$sufix);
    }
    else{
        //Peticiones POST para usuarios autorizados 
        if(isset($_GET["token"])){
            $tableToken = $_GET["table"];
            $sufix = $_GET["sufix"];
            $valido = Connection::tokenValido($_GET["token"],$tableToken,$sufix);
            if($valido == "Ok"){
                $response = new PostController();
                $response -> postData($table,$_POST);
            }
            if($valido == "Expirado"){
                $json = array(
                    'status' => 303,
                    'res' => 'Error El token ha expirado'
                );
                echo json_encode($json,http_response_code($json["status"]));
                return;
            }
            if($valido == "No autorizado"){
                $json = array(
                    'status' => 400,
                    'res' => 'Error El usuario no es autorizado'
                );
                echo json_encode($json,http_response_code($json["status"]));
                return;
            }
        }else{
            $json = array(
                'status' => 400,
                'res' => 'Autorización requerida'
            );
            echo json_encode($json,http_response_code($json["status"]));
            return;
        }
    }

}


?>