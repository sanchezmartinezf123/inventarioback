<?php

require_once("controllers/get.controller.php");

$table = explode("?", $routesArray[4])[0];

$select = $_GET["select"] ?? "*";
$orderBy = $_GET["orderBy"] ?? null;
$orderMode = $_GET["orderMode"] ?? null;
$startAt = $_GET["startAt"] ?? null;
$endAt = $_GET["endAt"] ?? null;
$gm = $_GET["gm"] ?? null;

$response = new GetController();

if (isset($_GET["linkTo"]) && isset($_GET["equalTo"]) && !isset($_GET["rel"]) && !isset($_GET["type"])) {
    $response -> getDataFilter(
        $table,
        $select,
        $_GET["linkTo"],
        $_GET["equalTo"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}

elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations"
        && !isset($_GET["linkTo"]) && !isset($_GET["equalTo"])) {
            $response -> getRelData(
                $_GET["rel"],
                $_GET["type"],
                $select,
                $orderBy,
                $orderMode,
                $startAt,
                $endAt
            );
}


elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations"
        && isset($_GET["linkTo"]) && isset($_GET["equalTo"])) {
            $response -> getRelDataFilter(
                $_GET["rel"],
                $_GET["type"],
                $select,
                $_GET["linkTo"],
                $_GET["equalTo"],
                $orderBy,
                $orderMode,
                $startAt,
                $endAt,
                $gm
            );
}

elseif (isset($_GET["linkTo"]) && isset($_GET["search"]) && !isset($_GET["rel"]) && !isset($_GET["type"])) {
    $response -> getDataSearch(
        $table,
        $select,
        $_GET["linkTo"],
        $_GET["search"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}


elseif (isset($_GET["rel"]) && isset($_GET["type"]) && $table == "relations" && isset($_GET["linkTo"]) && isset($_GET["search"])) {
    $response -> getRelDataSearch(
        $_GET["rel"],
        $_GET["type"],
        $select,
        $_GET["linkTo"],
        $_GET["search"],
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}


else{
    $response -> getData(
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
    );
}

?>