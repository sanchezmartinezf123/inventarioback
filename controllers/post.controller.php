<?php
require_once("models/post.Model.php");
require_once("models/get.Model.php");
require_once("models/put.Model.php");
require_once("models/connection.php");
require_once ("vendor/autoload.php");
use Firebase\JWT\JWT;
use Firebase\JWT\Key;

class PostController{

    static public function postData($table,$data){
        $response = PostModel::postData($table,$data);

        $return = new PostController();
        $return -> fncResponse($response,null);
    }

    static public function postRegister($table,$data,$sufix){

        if(isset($data["password_".$sufix]) && $data["password_".$sufix] != null){
            $crypt = crypt($data["password_".$sufix], '$2a$07$usesomesillystringforsalt$');
            $data["password_".$sufix] = $crypt;
            $response = PostModel::postData($table,$data);
            $return = new PostController();
            $return -> fncResponse($response,null);
        }
    }

    static public function postLogin($table,$data,$sufix){
        $response = GetModel::getDataFilter($table,"*","email_".$sufix,$data["email_".$sufix],null,null,null,null);
        if (!empty($response)) {
            $crypt = crypt($data["password_".$sufix], '$2a$07$usesomesillystringforsalt$');
            if ($response[0]->{"password_".$sufix} == $crypt) {
                $token = Connection::jwt($response[0]->{"id_".$sufix}, $response[0]->{"email_".$sufix});
                $key = 'key';
                $jwt = JWT::encode($token,$key,"HS256");
                //Actualizar la DB
                $data = array(
                    "token_".$sufix => $jwt,
                    "token_exp_".$sufix => $token["exp"]
                );

                $update = PutModel::putData($table,$data,$response[0]->{"id_".$sufix},"id_".$sufix);
                if (isset($update["comment"]) && $update["comment"] == "Dato Editado") {
                    $response[0]->{"token_".$sufix} = $jwt;
                    $response[0]->{"token_exp_".$sufix} = $token["exp"];
                    $return = new PostController();
                    $return -> fncResponse($response,null);
                }
                return;

            }else{
                $response = null;
                $return = new PostController();
                $return -> fncResponse($response,"Password malo");
            }
        }else{
            $response = null;
            $return = new PostController();
            $return -> fncResponse($response,"Email malo");
        }
    }

    public function fncResponse($response,$error){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                'res' => $response
            );
        }else {
            if ($error != null) {
                $json = array(
                    'status' => 404,   
                    'res' => $error
                ); 
            }else{
                $json = array(
                    'status' => 404,
                    'res' => 'Not found',
                    "method" => "POST"
                ); 
            }
        }

        echo json_encode($json,http_response_code($json["status"]));
    }
    

}

?>