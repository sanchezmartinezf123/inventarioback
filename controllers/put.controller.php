<?php
require_once("models/put.Model.php");

class PutController{

    static public function putData($table,$data,$id,$nameId){
        if ($data["password_usuario"]) {
            echo "Actualizando usuario";
            $crypt = crypt($data["password_usuario"], '$2a$07$usesomesillystringforsalt$');
            $data["password_usuario"] = $crypt;
            $response = PutModel::putData($table,$data,$id,$nameId);
            $return = new PutController();
            $return -> fncResponse($response);
        }else{
            $response = PutModel::putData($table,$data,$id,$nameId);
            $return = new PutController();
            $return -> fncResponse($response);
        }
    }

    public function fncResponse($response){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                'res' => $response
            );
        }else {
            $json = array(
                'status' => 404,
                'res' => 'Not found',
                "method" => "PUT"
            ); 
        }

        echo json_encode($json,http_response_code($json["status"]));
    }
    

}

?>