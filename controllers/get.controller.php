<?php
require_once("models/get.Model.php");

class GetController{    
    
    static public function getData(
        
        $table,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getData(
            $table,
            $select,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    static public function getDataFilter(
        $table,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getDataFilter(
            $table,
            $select,
            $linkTo,
            $equalTo,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    static public function getRelData(
        $rel,
        $type,
        $select,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getRelData(
            $rel,
            $type,
            $select,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    static public function getRelDataFilter(
        $rel,
        $type,
        $select,
        $linkTo,
        $equalTo,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt,
        $gm
        ){
        $response = GetModel::getRelDataFilter(
            $rel,
            $type,
            $select,
            $linkTo,
            $equalTo,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt,
            $gm
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }


    public function fncResponse($response){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                "total" => count($response),
                'res' => $response
            );
        }else {
            $json = array(
                'status' => 404,
                'res' => 'Not found'
            ); 
        }

        echo json_encode($json,http_response_code($json["status"]));
    }

    static public function getDataSearch(
        $table,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getDataSearch(
            $table,
            $select,
            $linkTo,
            $search,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

    static public function getRelDataSearch(
        $rel,
        $type,
        $select,
        $linkTo,
        $search,
        $orderBy,
        $orderMode,
        $startAt,
        $endAt
        ){
        $response = GetModel::getRelDataSearch(
            $rel,
            $type,
            $select,
            $linkTo,
            $search,
            $orderBy,
            $orderMode,
            $startAt,
            $endAt
        );
        $return = new GetController();
        $return -> fncResponse($response);
    }

}


?>