<?php
require_once("models/delete.Model.php");

class DeleteController{

    static public function deleteData($table,$id,$nameId){
        $response = DeleteModel::deleteData($table,$id,$nameId);
        $return = new DeleteController();
        $return -> fncResponse($response);
    }

    public function fncResponse($response){
        if (!empty($response)) {
            $json = array(
                'status' => 200,
                'res' => $response
            );
        }else {
            $json = array(
                'status' => 404,
                'res' => 'Not found',
                "method" => "DEl"
            ); 
        }

        echo json_encode($json,http_response_code($json["status"]));
    }
    

}

?>